import os
import pytest
import mock
from dict_tools import data


@pytest.mark.asyncio
async def test_get_osarch_uname(mock_hub, hub):
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict({"stdout": "test_arch"})

    with mock.patch("shutil.which", side_effect=[True]):
        mock_hub.grains.linux.os.arch.get_osarch = hub.grains.linux.os.arch.get_osarch
        await mock_hub.grains.linux.os.arch.get_osarch()

    assert mock_hub.grains.GRAINS.osarch == "test_arch"


@pytest.mark.asyncio
async def test_get_osarch_rpm(mock_hub, hub):
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict({"stdout": "test_arch"})

    with mock.patch.dict(os.environ, {"_host_cpu": "True"}):
        with mock.patch("shutil.which", side_effect=[False, True]):
            mock_hub.grains.linux.os.arch.get_osarch = (
                hub.grains.linux.os.arch.get_osarch
            )
            await mock_hub.grains.linux.os.arch.get_osarch()

    assert mock_hub.grains.GRAINS.osarch == "test_arch"


@pytest.mark.asyncio
async def test_get_osarch_opkg(mock_hub, hub):
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict(
        {"stdout": "foo\narch test_arch 64"}
    )

    with mock.patch("shutil.which", side_effect=[False, False, True]):
        mock_hub.grains.linux.os.arch.get_osarch = hub.grains.linux.os.arch.get_osarch
        await mock_hub.grains.linux.os.arch.get_osarch()

    assert mock_hub.grains.GRAINS.osarch == "test_arch"


@pytest.mark.asyncio
async def test_get_osarch_dpkg(mock_hub, hub):
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict({"stdout": "test_arch"})

    with mock.patch("shutil.which", side_effect=[False, False, False, True]):
        mock_hub.grains.linux.os.arch.get_osarch = hub.grains.linux.os.arch.get_osarch
        await mock_hub.grains.linux.os.arch.get_osarch()

    assert mock_hub.grains.GRAINS.osarch == "test_arch"


@pytest.mark.asyncio
async def test_get_osarch_6432(mock_hub, hub):
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict({"stdout": "test_arch"})

    with mock.patch("shutil.which", side_effect=[False, False, False, False]):
        with mock.patch.dict(os.environ, {"PROCESSOR_ARCHITEW6432": "test_arch"}):
            mock_hub.grains.linux.os.arch.get_osarch = (
                hub.grains.linux.os.arch.get_osarch
            )
            await mock_hub.grains.linux.os.arch.get_osarch()

    assert mock_hub.grains.GRAINS.osarch == "test_arch"


@pytest.mark.asyncio
async def test_get_osarch(mock_hub, hub):
    mock_hub.exec.cmd.run.return_value = data.NamespaceDict({"stdout": "test_arch"})

    with mock.patch("shutil.which", side_effect=[False, False, False, False]):
        with mock.patch.dict(os.environ, {"PROCESSOR_ARCHITECTURE": "test_arch"}):
            mock_hub.grains.linux.os.arch.get_osarch = (
                hub.grains.linux.os.arch.get_osarch
            )
            await mock_hub.grains.linux.os.arch.get_osarch()

    assert mock_hub.grains.GRAINS.osarch == "test_arch"
