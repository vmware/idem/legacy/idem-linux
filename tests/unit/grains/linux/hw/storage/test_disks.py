import glob
import io
import pytest
import mock

DISK_DATA = """
"""


@pytest.mark.asyncio
async def test_load_disks(mock_hub, hub):
    with mock.patch("os.path.exists", return_value=True):
        with mock.patch.object(
            glob,
            "glob",
            return_value=[
                f"/sys/blcok/test-disk{num}/queue/rotational" for num in range(6)
            ],
        ):
            with mock.patch(
                "aiofiles.threadpool.sync_open",
                side_effect=[
                    io.StringIO("0"),  # SSD
                    io.StringIO("0"),  # SSD
                    io.StringIO("0"),  # SSD
                    io.StringIO("1"),  # HDD
                    io.StringIO("1"),  # HDD
                    io.StringIO("1"),  # HDD
                ],
            ):
                mock_hub.grains.linux.hw.storage.disks.load_disks = (
                    hub.grains.linux.hw.storage.disks.load_disks
                )
                await mock_hub.grains.linux.hw.storage.disks.load_disks()

    assert mock_hub.grains.GRAINS.SSDs == ("test-disk0", "test-disk1", "test-disk2")
    assert mock_hub.grains.GRAINS.disks == ("test-disk3", "test-disk4", "test-disk5")
