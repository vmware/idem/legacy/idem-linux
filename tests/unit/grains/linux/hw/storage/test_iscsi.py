import io
import pytest
import mock


ISCSI_DATA = """
InitiatorName=iqn.2005-03.org.open-iscsi:3f5058b1d0a0
InitiatorName=iqn.2006-04.com.example.node1
"""


@pytest.mark.asyncio
async def test_load_iqn(mock_hub, hub):
    with mock.patch("os.path.exists", return_value=True):
        with mock.patch(
            "aiofiles.threadpool.sync_open", return_value=io.StringIO(ISCSI_DATA)
        ):
            mock_hub.grains.linux.hw.storage.iscsi.load_iqn = (
                hub.grains.linux.hw.storage.iscsi.load_iqn
            )
            await mock_hub.grains.linux.hw.storage.iscsi.load_iqn()

    assert mock_hub.grains.GRAINS.iscsi_iqn == (
        "iqn.2005-03.org.open-iscsi:3f5058b1d0a0",
        "iqn.2006-04.com.example.node1",
    )
