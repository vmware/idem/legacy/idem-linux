import os
import pytest


@pytest.fixture(scope="session")
def hub(hub):
    try:
        if os.uname().sysname == "Linux":
            return hub
    except:
        ...
    pytest.skip("idem-linux only runs on linux systems")
