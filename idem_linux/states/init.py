import os


def __sub_virtual__(hub):
    try:
        if os.uname().sysname == "Linux":
            return True
    except:
        ...
    return False, "idem-linux only runs on linux systems"
